import etcd3 as etcd
import time
import logging
logging.basicConfig(format='#%(levelname)s:%(message)s', level=logging.INFO)

client = etcd.Client('etcd1', 2379)
while True:
    time.sleep(3)
    logging.info(f'{client.range("rand")}')

