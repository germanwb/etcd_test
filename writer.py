import etcd3 as etcd
import logging
import datetime
import time
import random
logging.basicConfig(format='#%(levelname)s:%(message)s', level=logging.INFO)
client = etcd.Client('etcd3', 2379)
logging.info(f"{datetime.datetime.now()}")
logging.info(f"{client.version()}")
logging.info(f"{client.member_list()}")
n = 0
while True:
    resp = client.put(f'rand', f'{random.random()}')
    logging.info(f'{resp}')
    time.sleep(1)
